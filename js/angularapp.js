angular.module('AppName', ['ui.router'])

.config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('inicio');
    $stateProvider
        .state("home", {
            url: 'inicio',
            templateUrl : "another-view.html"
        })
        .state("other", {
            url: 'otro',
            templateUrl : "other-view.html"
        })
        .state("other2", {
            url: 'otro2/:id',
            templateUrl : "other-view2.html",
            controller: function ($scope,$stateParams) {
                $scope.param = $stateParams.id;
            }
        })
})

.controller('Controller1', function ($scope){
    $scope.simpleVar = 'simple text';
    // $scope es lo que necesitamos para comunicarnos entre la vista del controlador y las variables en
    // el controlador
})

.controller('TwoWayBinding', function ($scope) {
    $scope.someVar = 'Oscar';
    // Two way binding se conoce a la capacidad de actualizar las variables desde la vista y viceversa
    // Cuando cambia la variable en la vista o en el controlador se actualiza el valor en ambos lados
})

.controller ('CategoryFilter', function ($scope) {
    $scope.someData = [
        {id:0, name:'usr1', category:1},
        {id:1, name:'usr2', category:1},
        {id:3, name:'usr4', category:2},
        {id:5, name:'usr5', category:3},
        {id:6, name:'usr6', category:5},
        {id:23, name:'usr7', category:7}
    ];
    $scope.filter = '';
    $scope.unsetFilter = function () {
        $scope.filter = '';
    };
})

.factory('sharedData', function () {
    return {
        name: ''
    }
})

.controller('useFactory', function ($scope, sharedData) {
    $scope.name = sharedData.name;
    $scope.setName = function () {
        sharedData.name = $scope.name;
    }
})

.controller('useFactory2', function ($scope, sharedData) {
    $scope.name = sharedData.name;
    $scope.getName = function () {
        $scope.name = sharedData.name;
    }
})

.controller ('promises', function ($scope, $http, $q) {
    function getCountries () {
        var defered = $q.defer();
        var defered = $http.get('http://services.groupkt.com/country/get/all');
        return defered;
    };

    getCountries().then (function (data) {
        var result = angular.fromJson(data.data.RestResponse.result);
        $scope.countries =  (result);
    });
})
;